------------
Random Notes
------------

.. note::

  This file does not necessarily describe the current state, its just some random
  ideas that may or may not be implemented.

A table is a sequence of rows.
A row is a sequence of cells.
A column is, too.

We use the word `delimiter` for characters between adjacent columns, and `separator`
for characters between adjacent rows. No particular reason, it's just how it turned out.
A character may be a delimiter and a separator at the same time, in which case we call it
a `crossing`.

The `width` of a column includes any padding or potential delimiters on *either* side of the column.

Specifying delimiters/separators using pattern matching
-------------------------------------------------------

I had this idea before, but never actually tried it.
Let's say, all cells have a label consisting of a single symbol,
preferably something that can be represented as a single character.
Then we could use rules like this:


.. code:

  "HHH"   "H| "   "   "  "   "
  "==="   "=+ "   "---"  "-+ "
  "BBB"   "B| "   "HHH"  "H| "

A pattern rule is made from a 3x3 grid of symbols.
Of the nine symbols, four are input symbols:

.. code:

   "HH"   "H "     "  "   "  "
   "BB"   "B "     "HH"   "H "

And the remaining five are output symbols

.. code:

    " "    "|"      " "     " "
   "==="  "=+ "    "---"   "-+ "
    " "    "|"      " "     "|"


To encode a table like the one in the `example <example.coffee>`_, we need
three types of label: " ", "B" and "H". In theory there are 3^4=81 possible
combination of input symbols. But here we only use a few of them.

Also, we do not need the 5 output cells for each pattern, because the patterns will usually overlap.
It should be enough (again, in theory) to only examine the output symbol at the very center.
This should also eliminate the problem of over specification.

So... a rule would be a (partial) function r: I -> I -> I -> I -> O
Better still, I think we can "learn" this function from looking at
a schematic example like the one below. (Not "learn" as in machine learning, but
much, much more simple.)

.. code:
        "       "
        " ┌─┬─┐ "
        " │H│H│ "
        " ┝━┿━┥ "
        " │B│B│ "
        " ├─┼─┤ "
        " │B│B│ "
        " └─┴─┘ "
        "       "

Move a 9x9 "window" over the example, only sample the four corners (input) and the center (output).
Voila. Of course this only works if the example is consistent. If it is not, this is easy to detect.

"   H" -> "┌"
"  ││" -> "─"
