{analyze, layout, renderRow} = require "./src/index"

# flatmap. you *always* need it.
flatmap = (arr,f)-> arr.reduce ((arr0, elm,i)->arr0.concat f elm,i,arr ), []

# first setup some nice stupid test data.
text = """
  Neque porro quisquam est, qui
  dolorem ipsum, quia dolor sit,
  amet, consectetur, adipisci
  velit […].
"""

rows =[
  ["Tall", "Wide", "Medium"]
  [text, text, text]
  ["weird", "looking good", "still ok"]
]


# basic column specs: We want the first column
# to have a fixed width of 10 characters
# (including delimiters and padding).
# Note that we also configure a delimiter and padding.
columns = [
  {weight:0.0, minWidth:17, delimiter:"│", padding:" "}
  {weight:3.0,              delimiter:"│", padding:" "}
  {weight:1.0,              delimiter:"│", padding:" "}
]

# Determine the minWidth for the other two columns by looking
# at the actual content.
columns = columns.map analyze rows

# Determine how wide the whole table should be
desiredWidth = 80

# calculate the actual widths for each column, distributing any excess
# space according to the weights.
columns = layout desiredWidth, columns

# We also want some horizontal separator lines between
# table rows. Lets set this up
separators =
  top:
    crossing: ["┌","┬","┐"]
    hline: "─"
  header:
    crossing: ["┝","┿","┥"]
    hline: "━"
  default:
    crossing: ["├","┼","┤"]
    hline: "─"
  bottom:
    crossing: ["└","┴","┘"]
    hline: "─"

separatorPolicy = (row, rowIndex, rows)->
  switch rowIndex
    when 0 then ["top", "header"]
    when rows.length - 1 then [null, "bottom"]
    else [null, "default"]

# and finally, bring it all together

lines = flatmap rows, renderRow columns, separators, separatorPolicy

# and print it to the terminal
console.log lines.join "\n"
