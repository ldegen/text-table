
# Tables are difficult. Let's break it down into separate problems that look a
# little less daunting.
#
# - We need to find a "good" width for each column.
#
# - If we know the column widths, we can walk through the table cell by cell
#   and create rectangular blocks of text by wrapping and padding the content of
#   the cell.
#
# - We then have to put these blocks next to each other, applying vertical
#   padding to let them all have the same hight.
#
# - It should then be relatively simple to join the cells, adding
#   separator/border characters.
#
#
# To calculate the width of each column we can proceed as follows:
#
# - Each column has a natural minimal width: it is the width of the longest,
#   non-breakable token within this column (plus a bit of padding, let's say one
#   character at each side).
#
#   Picking a smaller width for the column makes no sense. On the other hand,
#   this natural minimum is probably a good pick for columns that do not
#   contain a large amount of text. In particular, if we combine this with some
#   way of expressing rules for non-breakable tokens, this should be
#   sufficient.
#
# - For columns that contain larger amounts of text we would like to allow
#   them to use up any space that may be "left". If there is more than one such
#   column, we can distribute the "left over" space using some weighting
#   mechanism.
#
#   Adding the minimal widths of all columns will give us the minimal width of
#   the whole table.  If we additionally specify a "desired width" for the table
#   that is larger than the minimum we can substract the minimum to obtain the
#   space that is "left over".
#
flatmap = (arr,f)-> arr.reduce ((arr0, elm,i)->arr0.concat f elm,i,arr ), []
sum = (a,b)->a+b

unindent = (lines)=>
  if typeof lines is 'string'
    return unindent(lines.split('\n')).join('\n')
  min = lines.reduce ((min,line)->
    if line.trim().length is 0
      return min
    Math.min(min, line.match(/\S/).index)
  ), Number.POSITIVE_INFINITY
  lines.map (line) ->
    line.substring(min)


# normalizeColumnSettings is used in various places to make sure
# a given column descriptor is complete and consistent. It calculates
# derived attributes, and fills in missing attributes using defaults.
normalizeColumnSettings = (colOpts, colNum=colOpts.columnIndex)->

  {
    tokenizer:tokenizer0,
    minWidth,
    padding="",
    delimiter="",
    splitParagraphs,
    paragraphDecoration
  } = colOpts

  tokenizer = if tokenizer0
    (s)->
      r=tokenizer0 s
      if Array.isArray(r) then r else [r]
  else
    (s)->s.split /\s+/

  splitParagraphs ?= (par, parIndex) ->
    par.split /\n(?:\s*\n)+/
  paragraphDecoration ?= (par, parIndex) ->
    indentRegexp = /^\s*([\-\*] )?/
    firstIndent = par.match(indentRegexp)[0]
    decoWidth = firstIndent.length
    normalIndent = ' '.repeat(decoWidth)
    undecorateLine = (line, i)->
      line.substring(line.match(indentRegexp)[0].length)
    redecorateLine = (line, i)->
      if i is 0 then firstIndent + line else normalIndent + line
    [undecorateLine, redecorateLine, decoWidth]


  padding = [padding, padding] if typeof padding is "string"
  delimiter = [delimiter,delimiter] if typeof delimiter is "string"
  delimiter[0] = "" if colNum > 0

  paddingWidth = padding
    .map (s)->s.length
    .reduce sum,0

  delimiterWidth = delimiter
    .map (s)->s.length
    .reduce sum,0

  start = delimiter[0] + padding[0]
  end = padding[1] + delimiter[1]

  {
    colOpts...,
    tokenizer,
    padding,
    delimiter,
    paddingWidth,
    delimiterWidth,
    start,
    end,
    columnIndex:colNum,
    paragraphDecoration,
    splitParagraphs
  }


# analyze is used to calculate the minWidth requirements of a column by looking at the actual data
# it needs to display.
#
# The minWidth value is later used by the layout function to determine the actual width of
# each column.
#
# As most of the other steps, this is completely optional. If you *know* the minWidth for your
# columns, just specify it yourself. Also note that the analyze function will never overwrite
# an already existing minWidth value, so you can set the value for some columns and let analyze
# figure only the remaining columns.
# Of couse, if you plan on using fixed widths for all columns, you do not need `layout`, and
# then there is also no point in using `analyze`.
#
# You might be wondering about the strange signature of the function. I know I do, whenever I return
# to this code after a couple of months.
# The rational is this: Given a set of actual data rows, this HOF derives an *augmentation* strategy
# for your column specs. This way, it can easily work with Array::map:
#
#   columns = columns.map analyze rows
#

analyze = (rows)->(colOpts, colNum)->
  colOpts = normalizeColumnSettings colOpts, colNum
  {tokenizer, minWidth, paddingWidth, delimiterWidth, splitParagraphs, paragraphDecoration} = colOpts
  minWidth = switch
    when typeof minWidth is "number" then minWidth
    when typeof minWidth is "function" then minWidth rows, colNum
    else
      cells = rows
        .map (row)->row[colNum] ? ""
        .map unindent
      paragraphs = cells.flatMap(splitParagraphs)
      contentWidth = paragraphs.reduce ((l, par)=>
        [_,_,decoWidth] = paragraphDecoration par
        tokens = tokenizer par
        tokens.reduce ((l, token)->Math.max l, token.length + decoWidth), l
      ), 0

      contentWidth + paddingWidth + delimiterWidth

  {colOpts..., minWidth}

# layout is another preprocessing step. For each column, it will look at the minWidth and weight
# attributes, and it will then calculate concrete width values for each column.
# The process works like this:
# First, the minimum width for the whole table is calculated by summing the minWidth values
# of each column. The difference between the desiredWidth and the minimal width is the total
# extra space that is distributed among the columns. The distribution will try to do this
# according to the individual `weight` attributes of each column, so if you want a column
# width to be exactly the minWidth, use a weight of zero.
#
# Now, after normalizing the weights (dividing each weight by the sum of all weights),
# we could, in theory, just multiply a columns normalized weight with the amount of extra space
# add this to the column's minWidth and be done. But. We need to work with integral width values.
# (a width of some fraction of a character just makes no sense on a text display).
# So what we need is some kind of sum preserving rounding.
# I found one that is simple enough and works quite well:
#
# - initialy start by floring all summands.
# - determine the difference between the sum of the floored sumands and the original sum.
#   Note that the latter is an integral value. We call this difference the "error".
# - for as long as the error is > 0, find the summand with the greatest individual difference
#   between the original summand and its current approximation and increment the approximation.
#
# Can the error become negative? I.e. can the sum of our approcimations exceed the original sum?
# No. Both somes are positive integers. The approximation is initially smaller than the original.
# Each iteration adds excalty 1 to the sum of approximations.
#
# Can any of individual approximations exceed the corresponding original summand?
# Yes. But it will never exceed ceil(original_summand). Each approximation starts at
# floor(original summand). If it is the one with the largest individual error, it gets incremented
# and will then be ceil(original_summand). After that it will not be incremented another time,
# because it has a negative error and all either the overall error is zero or another
# individual approximation exists with a non-negative error.
#
# So, yay: it works :-)
layout = (desiredWidth, columns)->
  minWidths = columns.map (c)->c.minWidth ? 0
  minTableWidth = minWidths.reduce sum ,0
  excessWidth = desiredWidth - minTableWidth
  sumWeights = columns
    .map (c)->c.weight ? 0
    .reduce sum, 0
  quotas = columns
    .map ({weight=0}, i) -> weight * excessWidth / sumWeights

  flooredQuotas = quotas.map Math.floor

  error=excessWidth - flooredQuotas.reduce(sum,0)
  while error isnt 0
    # I think the error can only be positive. Let's make sure I am not too tired.
    throw "I should go to bed now." unless error > 0

    reducer = (iMax, floored, i)->
      maxError = quotas[iMax] - flooredQuotas[iMax]
      curError = quotas[i] - flooredQuotas[i]

      throw "Really. It is much to late." if maxError < 0 or curError < 0

      if curError > maxError then i else iMax
    j=flooredQuotas.reduce reducer, 0
    flooredQuotas[j]++
    error=excessWidth - flooredQuotas.reduce(sum,0)

  # now, all that's left is adding the minWidths to the flooredQuotas
  minWidths.map (mw,i)->{columns[i]..., width: mw+flooredQuotas[i]}

renderParagraph = (columnLayout)->
  columnLayout = normalizeColumnSettings columnLayout

  (par, parIndex)->
    {width, tokenizer, start, end, paddingWidth, delimiterWidth, paragraphDecoration} = columnLayout
    [undecorate, redecorate, decoWidth] = paragraphDecoration(par, parIndex)
    contentWidth = width - paddingWidth - delimiterWidth - decoWidth
    pad = (s,lineNum)->start + redecorate(s.padEnd(contentWidth), lineNum) + end
    empty = start + ' '.repeat(contentWidth+decoWidth) + end
    unpar = par.split('\n').map(undecorate).join('\n')
    tokens = tokenizer unpar

    lines = []
    buf = ""
    buflen = 0
    while tokens.length > 0
      if buflen is 0
        # special case: buf is empty. we do not need to check the
        # length of the next token, because we know it cannot be wider then contentWidth.
        tokens.shift() while tokens.length > 0 and tokens[0] is "\n"
        token = tokens.shift()
        buflen += token.length
        buf += token
      else if tokens[0] is "\n"
        # special case: the tokenizer explicitly asks for a line break.
        lines.push pad(buf,lines.length)
        buf = ""
        buflen = 0
      else if buflen + tokens[0].length < contentWidth
        # otherwise we check if an extra space plus the next token can be appended
        token = tokens.shift()
        buf += " "+token
        buflen += token.length + 1
      else
        # if it does not fit, commit the buffer and start a new line.
        lines.push pad(buf, lines.length)
        buf = ""
        buflen = 0
    # flush the buffer it is isnt empty.
    if buflen > 0
      lines.push pad(buf, lines.length)

    if parIndex > 0
      lines.unshift empty
    lines

# For a given column specification, this will create a renderer for a single cell.
# This includes wrapping the cell content at token borders, adding any configured
# leading and/or trailing delimiters and or padding. It does not, however, do
# any vertical padding or separators. For this, see `fillCell` and `rowSeparator`
# respectively.
renderCell = (columnLayout)->
  {splitParagraphs} = normalizeColumnSettings columnLayout
  (cell)->
    paragraphs = splitParagraphs(unindent(cell))
    paragraphs.flatMap(renderParagraph(columnLayout))

# this adds vertical padding to an already rendered cell.
# Given a column spec, it creates a vertial padding ("fill")-strategy for
# individual cell. The strategy takes the desired height of the row aswell as
# the already rendered lines (output of renderCell) and appends empty lines
# (including any configured column delimiters) so that the output contains
# exactly rowHeight lines, but only if rowHeight is strictly larger than the
# the already rendered number of lines.
fillCell = (columnLayout)->
  columnLayout = normalizeColumnSettings columnLayout
  (rowHeight, lines)->
    if lines.length >= rowHeight then lines
    else
      {paddingWidth, delimiterWidth, width, start, end} = columnLayout
      contentWidth = width - paddingWidth - delimiterWidth
      emptyLine = start + ''.padEnd(contentWidth) + end
      emptyLines = (emptyLine for i in [0...(rowHeight - lines.length)])
      lines.concat emptyLines

# this is used to render separator lines before, after or inbetween rows.
# Given a separator configuration (see below), this will create a render strategy
# for a single column spec. Like most functions in this library, this is
# designed so that the strategy can be bassed to Array::map.
#
# A separator configuration is an object with two options, `crossing` and `hline`.
# Both options are optional.
#
# `crossing` should be a string or an array of three strings. Each should contain the
# character to be used for a left-most, an inner, and a right-most crossing respectively.
# So for example, if you were to draw a separator between two rows, you would use something like
#
#   crossing: ["├","┼","┤"]
#
# whereas, for drawing the bottom border of the whole table, you would do
#
#   crossing: ["└","┴","┘"]
#
# The `hline` value should be a single string containing the character to draw the
# horizontal line segments connecting the crossings, so in the above examples,
# something like  `hline: "─"`  would probably make sense.

renderSeparator = ({crossing='+', hline='-'})->
  crossing = [crossing, crossing, crossing] if typeof crossing is "string"
  (columnLayout, i,columnLayouts)->
    {width, columnIndex, delimiter, delimiterWidth} = normalizeColumnSettings columnLayout
    hlineWidth = width - delimiterWidth
    s = ""
    if columnIndex is 0
      s += "".padEnd delimiter[0].length, crossing[0]
      s += "".padEnd(hlineWidth, hline)
      s += "".padEnd delimiter[1].length, crossing[1]
    else if columnIndex is columnLayouts.length - 1
      s += "".padEnd(hlineWidth, hline)
      s += "".padEnd delimiter[1].length, crossing[2]
    else
      s += "".padEnd(hlineWidth, hline)
      s += "".padEnd delimiter[1].length, crossing[1]
    s

max = (a,b)->Math.max(a,b)
zip = (arrays..., f)->
  N = arrays
    .map (arr)->arr.length
    .reduce max, 0
  for i in [0...N]
    args = arrays.map (arr)->arr[i]
    f args...

# this this orchestrates the actual rendering of a table row.
# You need to do the preprocessing steps (anaylze, layout) as
# you see fit, renderRow will then
# do the rest, i.e. use renderCell, fillCell, renderSeparator in
# the apropriate ways.
#
# Again, the signature of this function was choosen to allow it to work nicely with
# Array::map, that is, given columnSpecs, separators and a separator policy, it
# will produce a render strategy that you can pass as an argument to Array::map
# to process your data rows.
#
# the optional arguments separators and separatorPolicy are used to decide
# wether a row separator needs to be drawn above and/or below it, and which separator
# chars to use.
#
# separators should be an object of separator configurations (see renderSeparator
# for details).
#
# The separatorPolicy is called with the same arguments that are passed into
# the render strategy, typically by Array::map: the current row, the rowIndex and the
# array of all known rows.
#
# The separatorPolicy should return an array of two elements, each one should be either a
# key into the separators object, or null. The first one will determine the configuration
# for a separator line above the current line, the second for one below the current line.
# In both cases, if the respective element is null or undefined, no separator line will be
# drawn.
#
# If you need more control, of course nothing is stopping your from manually interleaving
# calls to renderRow and renderSeparator. This library was intentionally designed to
# give you that freedom, just do whatever works best for *your* use case.
#
renderRow = (columnLayouts, separators={}, separatorPolicy)->
  columnLayouts = columnLayouts.map normalizeColumnSettings

  renderers = columnLayouts.map renderCell
  fillers = columnLayouts.map fillCell

  separatorPolicy ?= ->[null,null]

  cachedSeparators = {}
  for key,sepOpts of separators
    cachedSeparators[key] = columnLayouts.map(renderSeparator(sepOpts)).join ""


  (cells, rowIndex, rows)->

    # render the cells, pass one
    linesPerCell = zip renderers, cells, (render, cell)->render cell

    # determine rowHeight
    rowHeight = linesPerCell
      .map (lines)->lines.length
      .reduce max, 0

    # apply vertical padding (pass two)
    linesPerCell = zip fillers, linesPerCell, (fill, cell)->fill rowHeight, cell

    # concatenate the lines
    lines = zip linesPerCell..., (parts...)->parts.join ""

    [aboveKey, belowKey] = separatorPolicy cells, rowIndex, rows
    above = if aboveKey? then cachedSeparators[aboveKey] ? cachedSeparators.default
    below = if belowKey? then cachedSeparators[belowKey] ? cachedSeparators.default

    lines.unshift above if above?
    lines.push below if below?

    lines

module.exports = {analyze, layout, renderCell, fillCell, renderSeparator, renderRow}


