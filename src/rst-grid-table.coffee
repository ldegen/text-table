{analyze, layout, renderRow} = require "./table"
flatmap = (arr,f)-> arr.reduce ((arr0, elm,i)->arr0.concat f elm,i,arr ), []

rstGridTable = ({desiredWidth=80, columns}, rows)->
  columns = columns.map (col)->{col..., delimiter:"|", padding:" "}
  columns = layout desiredWidth, columns.map analyze(rows)

  separators =
    header:
      crossing: "+"
      hline: "="
    default:
      crossing: "+"
      hline: "-"


  separatorPolicy = (row, rowIndex, rows)->
    switch rowIndex
      when 0 then ["top", "header"]
      when rows.length - 1 then [null, "bottom"]
      else [null, "default"]
  s=flatmap rows, renderRow(columns, separators, separatorPolicy)
    .join "\n"
  s + "\n"
module.exports = rstGridTable
