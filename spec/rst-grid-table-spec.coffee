rstGridTable = require "../src/rst-grid-table.coffee"
describe "rstGridTable", ->

  it "is a convenient short-cut for rendering reStructuredText Grid Tables", ->

    text = """
      Neque porro quisquam est, qui 
      dolorem ipsum, quia dolor sit, 
      amet, consectetur, adipisci 
      velit […].
    """

    rows =[
      ["Tall", "Wide", "Medium"]
      [text, text, text]
      ["weird", "looking good", "still ok"]
    ]

    opts =
      desiredWidth: 80
      columns: [
        weight:0
      ,
        weight:1.5
      ,
        weight:1
      ]

    expect(rstGridTable opts, rows).to.equal """
    +--------------+----------------------------------+----------------------------+
    | Tall         | Wide                             | Medium                     |
    +==============+==================================+============================+
    | Neque porro  | Neque porro quisquam est, qui    | Neque porro quisquam est,  |
    | quisquam     | dolorem ipsum, quia dolor sit,   | qui dolorem ipsum, quia    |
    | est, qui     | amet, consectetur, adipisci      | dolor sit, amet,           |
    | dolorem      | velit […].                       | consectetur, adipisci      |
    | ipsum, quia  |                                  | velit […].                 |
    | dolor sit,   |                                  |                            |
    | amet,        |                                  |                            |
    | consectetur, |                                  |                            |
    | adipisci     |                                  |                            |
    | velit […].   |                                  |                            |
    +--------------+----------------------------------+----------------------------+
    | weird        | looking good                     | still ok                   |
    +--------------+----------------------------------+----------------------------+

    """
