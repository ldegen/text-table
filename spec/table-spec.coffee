{analyze, layout, renderCell, fillCell, renderRow, renderSeparator} = require "../src/index.coffee"

flatmap = (arr,f)-> arr.reduce ((arr0, elm,i)->arr0.concat f elm,i,arr ), []
describe "Table Formatting Utilities", ->


  # analyze : Data -> ColConfig -> ColIx -> ColReq
  describe "analyze", ->
    it "determines the minimal width for a given column", ->
      data = [
        ["several words within, containing , separator chars-and-more" ]
        [" it is pretty stupid .....,,,,,,,,because it only-cares for whitespace" ]
      ]
      {minWidth, columnIndex}= analyze(data) {}, 0
      expect(minWidth).to.equal ".....,,,,,,,,because".length
      expect(columnIndex).to.equal 0

    it "can use custom tokenizers", ->
      # here is a simple example that will just prevent wrapping
      data = [
        ["one long token"]
        ["and an even longer token"]
        ["a"]
        ["surprisingly"]
        ["short"]
        ["token"]
      ]
      {minWidth, columnIndex} = analyze(data) {tokenizer:(s)->s}, 0
      expect(minWidth).to.equal "and an even longer token".length
      expect(columnIndex).to.equal 0

    it "will preserve any existing minWidth value", ->
      # this is mainly useful for mocking in the other test cases
      data = [
        ["it does not"]
        ["matter"]
        ["in"]
        ["the slightest"]
      ]
      {minWidth,columnIndex} = analyze(data) {minWidth:42}, 0
      expect(minWidth).to.equal 42
      expect(columnIndex).to.equal 0

    # each column may choose to draw a delimiter to its right.
    # The first column may also choose to draw a delimiter to its left.
    # A column may also decide to add padding to both the left and/or the right side.
    # The column must take all this into account when calculating its minimum width.
    it "takes column delimiters and padding into account", ->
      data = [
        ["does"   , "not"  , "really"]
        ["matter" , "that" , "much"  ]
      ]
      cols = [
        {delimiter:['||','|'], padding:["::","*"]}
        {padding:" "}
        {delimiter:'|'}
      ]
      actual = cols
        .map analyze(data)
        .map ({minWidth,columnIndex})->[columnIndex, minWidth]
      expect(actual).to.eql [
        [0,"||::matter*|".length]
        [1," that ".length]
        [2,"really|".length]
      ]


  # layout : [ColReq] -> DesiredWidth -> [ColLayout]
  describe "layout", ->
    it "produces an array of column layouts", ->
      columnRequirements = [
        {weight:0, minWidth: 14}
        {weight:2, minWidth: 5}
        {weight:1, minWidth: 6}
      ]
      desiredWidth = 50
      # there are 25 chars left over, of which the first column
      # gets 0, the second gets 25*2/3 = 16.666... and the third one 25/3 = 8.333...
      # But how do we deal with rounding here? We need some kind sum preserving rounding.
      #
      # The simplest one I have found so far goes like this:
      # - initialy start by floring all summands. In our example that would be 0, 16 and 8.
      # - summing these yields 24, so we have an error of 1
      # - for as long as there is an error, find the summand with the greatest difference
      #   to the unrounded value and increment (if the sum is to small) or decrement
      #   (if the sum is to large).
      #
      # In our case, we would clome out with 0, 17 and 8.
      actual = layout desiredWidth, columnRequirements
        .map ({width})->width
      expect(actual).to.eql [14+0,5+17,6+8]

  # renderCell : ColLayout -> Cell -> [Line]
  describe "renderCell", ->
    it "renders a single cell, producing an array of lines", ->
      columnLayout =
        width:32
        delimiter:"|"
        padding:" "
        columnIndex: 0
      cell = "The quick brown fox jumped over the lazy dog"
      actual = renderCell(columnLayout) cell
      expect(actual).to.eql [
        "| The quick brown fox jumped   |"
        "| over the lazy dog            |"
      ]


    it "preserves existing paragraphs", ->

      columnLayout =
        width:32
        delimiter:"|"
        padding:" "
        columnIndex: 0
      cell = """
      The quick brown fox

        - jumped over the lazy dog

        - made a turn, jumped back on the couch, leaving the dog somewhat puzzled and enraged.
        
          It has to be said: We can have paragraphs that are not
          bullet points, but indented nonetheless.
          
      """
      actual = renderCell(columnLayout) cell
      expect(actual).to.eql [
        "| The quick brown fox          |"
        "|                              |"
        "|   - jumped over the lazy dog |"
        "|                              |"
        "|   - made a turn, jumped back |"
        "|     on the couch, leaving    |"
        "|     the dog somewhat puzzled |"
        "|     and enraged.             |"
        "|                              |"
        "|     It has to be said: We    |"
        "|     can have paragraphs that |"
        "|     are not bullet points,   |"
        "|     but indented             |"
        "|     nonetheless.             |"
      ]

  # fillCell : ColLayout -> RowHeight -> [Line] -> [Line]
  describe "fillCell", ->
    it "adds emptyLines to a cell to fill a given row height", ->
      columnLayout =
        width:32
        delimiter:"|"
        padding:" "
        columnIndex: 0
      cell = "The quick brown fox jumped over the lazy dog"
      input = [
        "| The quick brown fox jumped   |"
        "| over the lazy dog            |"
      ]
      actual = fillCell(columnLayout) 4, input
      expect(actual).to.eql [
        "| The quick brown fox jumped   |"
        "| over the lazy dog            |"
        "|                              |"
        "|                              |"
      ]

  # renderSeparator : SeparatorConfig -> ColLayout -> String
  describe "rowSeparator", ->
    it "draws a separator row for a given list of column layouts", ->

      # note that each column includes the column separator to its right, and that
      # the first column also includes the column separator to its left.
      columnLayouts = [5,25,10].map (w,i)->width:w, columnIndex:i, delimiter:"|"
      expect(columnLayouts.map(renderSeparator({})).join "").to.eql "+---+------------------------+---------+"


    it "supports using custom characters", ->

      opts =
        crossing: ["├","┼","┤"]
        hline: "─"
      columnLayouts = [5,25,10].map (w,i)->width:w, columnIndex:i,delimiter:"│"
      expect(columnLayouts.map(renderSeparator(opts)).join "").to.eql "├───┼────────────────────────┼─────────┤"

    it "only renders crossings where the column uses delimiters", ->

      opts =
        crossing: ["├","┼","┤"]
        hline: "─"
      columnLayouts =[
        delimiter: ["","│"]
        width: 5
      ,
        width: 25
      ,
        width: 10
      ]

      expect(columnLayouts.map(renderSeparator(opts)).join "").to.eql "────┼───────────────────────────────────"

  # renderRow : [ColLayout] -> [Cell] -> RowIndex -> [Line]
  describe "renderRow", ->
    it "renders a single row", ->

      text = """
        Neque porro quisquam est, qui
        dolorem ipsum, quia dolor sit,
        amet, consectetur, adipisci
        velit […].
      """

      row = [text, text, text]

      columnLayouts = [16,39,25].map (w,i)->width:w, columnIndex:i,delimiter:"│",padding:" "


      expect(renderRow(columnLayouts) row).to.eql [
        "│ Neque porro  │ Neque porro quisquam est, qui        │ Neque porro quisquam   │"
        "│ quisquam     │ dolorem ipsum, quia dolor sit, amet, │ est, qui dolorem       │"
        "│ est, qui     │ consectetur, adipisci velit […].     │ ipsum, quia dolor sit, │"
        "│ dolorem      │                                      │ amet, consectetur,     │"
        "│ ipsum, quia  │                                      │ adipisci velit […].    │"
        "│ dolor sit,   │                                      │                        │"
        "│ amet,        │                                      │                        │"
        "│ consectetur, │                                      │                        │"
        "│ adipisci     │                                      │                        │"
        "│ velit […].   │                                      │                        │"
      ]

    it "uses a separator policy to decide what separators to include", ->

      text = """
        Neque porro quisquam est, qui
        dolorem ipsum, quia dolor sit,
        amet, consectetur, adipisci
        velit […].
      """

      rows =[
        ["Tall", "Wide", "Medium"]
        [text, text, text]
        ["weird", "looking good", "still ok"]
      ]

      separators =
        top:
          crossing: ["┌","┬","┐"]
          hline: "─"
        header:
          crossing: ["┝","┿","┥"]
          hline: "━"
        default:
          crossing: ["├","┼","┤"]
          hline: "─"
        bottom:
          crossing: ["└","┴","┘"]
          hline: "─"


      separatorPolicy = (row, rowIndex, rows)->
        switch rowIndex
          when 0 then ["top", "header"]
          when rows.length - 1 then [null, "bottom"]
          else [null, "default"]
      columnLayouts = [16,39,25].map (w,i)->width:w, columnIndex:i,delimiter:"│",padding:" "


      expect(flatmap rows, renderRow(columnLayouts, separators, separatorPolicy)).to.eql [
        "┌──────────────┬──────────────────────────────────────┬────────────────────────┐"
        "│ Tall         │ Wide                                 │ Medium                 │"
        "┝━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━┥"
        "│ Neque porro  │ Neque porro quisquam est, qui        │ Neque porro quisquam   │"
        "│ quisquam     │ dolorem ipsum, quia dolor sit, amet, │ est, qui dolorem       │"
        "│ est, qui     │ consectetur, adipisci velit […].     │ ipsum, quia dolor sit, │"
        "│ dolorem      │                                      │ amet, consectetur,     │"
        "│ ipsum, quia  │                                      │ adipisci velit […].    │"
        "│ dolor sit,   │                                      │                        │"
        "│ amet,        │                                      │                        │"
        "│ consectetur, │                                      │                        │"
        "│ adipisci     │                                      │                        │"
        "│ velit […].   │                                      │                        │"
        "├──────────────┼──────────────────────────────────────┼────────────────────────┤"
        "│ weird        │ looking good                         │ still ok               │"
        "└──────────────┴──────────────────────────────────────┴────────────────────────┘"
      ]



