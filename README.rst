----------
text-table
----------

:author: Lukas Degener
:title: text-table
:subtitle: Yet another JavaScript library for producing plain-text tables.

Install
=======

The usual ``npm install --save @l.degener/text-table`` should do.

Usage
=====

A rather minimalistic example might look like this:

.. code:: coffee

  {analyze, layout, renderRow} = require "./src/index"

  # flatmap. You *always* need it.
  flatmap = (arr,f)-> arr.reduce ((arr0, elm,i)->arr0.concat f elm,i,arr ), []

  # first setup some nice stupid test data.
  text = """
    Neque porro quisquam est, qui
    dolorem ipsum, quia dolor sit,
    amet, consectetur, adipisci
    velit […].
  """

  rows =[
    ["Tall", "Wide", "Medium"]
    [text, text, text]
    ["weird", "looking good", "still ok"]
  ]

  console.log flatmap(rows, renderRow(layout(80, [{weight:0},{weight:3},{weight:1}].map(analyze(rows))))).join "\n"

This should produce the following, rather unimpressive output:

.. code::

  Tall        Wide                                         Medium
  Neque porro Neque porro quisquam est, qui dolorem ipsum, Neque porro quisquam
  quisquam    quia dolor sit, amet, consectetur, adipisci  est, qui dolorem ipsum,
  est, qui    velit […].                                   quia dolor sit, amet,
  dolorem                                                  consectetur, adipisci
  ipsum, quia                                              velit […].
  dolor sit,
  amet,
  consectetur,
  adipisci
  velit […].
  weird       looking good                                 still ok

In `this example <example.coffee>`_ you can find a more complete variant that would render the same
data like this:

.. code::

  ┌───────────────┬───────────────────────────────────────┬──────────────────────┐
  │ Tall          │ Wide                                  │ Medium               │
  ┝━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━┥
  │ Neque porro   │ Neque porro quisquam est, qui dolorem │ Neque porro quisquam │
  │ quisquam est, │ ipsum, quia dolor sit, amet,          │ est, qui dolorem     │
  │ qui dolorem   │ consectetur, adipisci velit […].      │ ipsum, quia dolor    │
  │ ipsum, quia   │                                       │ sit, amet,           │
  │ dolor sit,    │                                       │ consectetur,         │
  │ amet,         │                                       │ adipisci velit […].  │
  │ consectetur,  │                                       │                      │
  │ adipisci      │                                       │                      │
  │ velit […].    │                                       │                      │
  ├───────────────┼───────────────────────────────────────┼──────────────────────┤
  │ weird         │ looking good                          │ still ok             │
  └───────────────┴───────────────────────────────────────┴──────────────────────┘

(This probably looks best in an actual text terminal using a bitmap font. But you get the idea.)

Why, oh why?!
=============

I had the following problem

-  I need text in some cells to be wrapped. In others not.
-  I need some simple adaptive column-width calculation.
-  I need the output to be `compatible with reStructuredText’s tables <spec/rst-grid-table-spec.coffee>`_.
-  I don’t want any more dependencies.

There are a lot of existing libraries that *almost* do what I need. Most
of them will be more robust, better performing and more feature complete
than this one.

But in the end, the whole problem just seemed like a fun challenge, so I
decided to try it myself.

So… sorry for creating yet another table formatting library. I just
couldn’t help myself.
